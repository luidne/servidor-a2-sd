package br.com.servidormusica.provedor;

import java.io.File;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Luidne
 */
@Path("/musica")
public class ProvedorMusica {
    
    @GET
    @Path("/id/{id_musica}")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    public Response getPorId(@PathParam("id_musica") int id){
        File file  = new File("f:/Dominic Balli - Again and Again DJ PV (Remix) - YouTube.mp3");
        
        return Response.ok(file).header("Content-Disposition", "attachment; filename=" + file.getName()).build();
    }
    
    @GET
    @Path("/tudo/")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    public Response getTudo(){
        File file  = new File("f:/");
        
        for(File f : file.listFiles()){
            System.out.println("Nome: "+f.getName());
        }
        
        return Response.ok(file).header("Content-Disposition", "attachment; filename=" + file.getName()).build();
    }
    
}
